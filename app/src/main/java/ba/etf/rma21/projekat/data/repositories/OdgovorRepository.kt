package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.Odgovor


class OdgovorRepository {
    companion object {
        private lateinit var context: Context
        fun setContext(_context:Context){
            context=_context
        }
        suspend fun getOdgovoriKviz(idKviza: Int): List<Odgovor>? {
            return emptyList()
        }

        suspend fun postaviOdgovorKviz(idKvizTaken: Int, idPitanje: Int, odgovor: Int): Int? {
            return null
        }
    }


}