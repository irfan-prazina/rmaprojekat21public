package ba.etf.rma21.projekat.data.repositories

import android.content.Context


class AccountRepository {

    companion object {
        //TODO Ovdje trebate dodati hash string vašeg accounta
        var acHash: String = ""
        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }
        fun postaviHash(acHash: String): Boolean {
            this.acHash = acHash
            return true
        }

        fun getHash(): String {
            return acHash
        }


    }
}